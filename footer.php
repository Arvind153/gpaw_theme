<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
 				<?php $footer_background_colour = get_field('footer_background_colour', 'options'); ?>
				<footer class="footer <?php echo $footer_background_colour; ?>" role="contentinfo">
					<ul class="accordion did-you-know" data-accordion data-allow-all-closed="true">
						<li class="accordion-item" data-accordion-item>
							<a href="#" class="accordion-title">
								<span class="image">
									<?php 

										$extensions = array("svg");
										$file_ext = strtolower(end(explode('.', get_field('did_you_know', 'options')['icon']['url'])));
										if(in_array($file_ext, $extensions) === true){
											$arrContextOptions=array(
											    "ssl"=>array(
											        "verify_peer"=>false,
											        "verify_peer_name"=>false,
											    ),
											);  

											$svg_file = file_get_contents(get_field('did_you_know', 'options')['icon']['url'], false, stream_context_create($arrContextOptions));
											$find_string   = '<svg';
											$position = strpos($svg_file, $find_string);
											$svg_file_new = substr($svg_file, $position);

											echo $svg_file;
										} 
									?>
								</span>

								<span class="text"><?php echo get_field('did_you_know', 'options')['title']; ?></span>
							</a>
							<div class="accordion-content" data-tab-content>
								<div class="fact">
									<?php
									  $input = get_field('did_you_know', 'options')['facts'];
									  $value = $input[array_rand($input)];
									  echo $value['fact'];
									?>
								</div>
								<a class="shuffle">
									<?php
										$extensions = array("svg");
										$file_ext = strtolower(end(explode('.', get_field('did_you_know', 'options')['dice_icon']['url'])));
										if(in_array($file_ext, $extensions) === true){
											$arrContextOptions=array(
											    "ssl"=>array(
											        "verify_peer"=>false,
											        "verify_peer_name"=>false,
											    ),
											);  

											$svg_file = file_get_contents(get_field('did_you_know', 'options')['dice_icon']['url'], false, stream_context_create($arrContextOptions));
											$find_string   = '<svg';
											$position = strpos($svg_file, $find_string);
											$svg_file_new = substr($svg_file, $position);

											echo $svg_file;
										} 
									?>
								</a>
							</div>
						</li>
					</ul>

					<?php $facts = array(); ?>
					<?php if(have_rows('did_you_know', 'options')) : ?>
						<?php while (have_rows('did_you_know', 'options')) : the_row(); ?>
							<?php if(have_rows('facts')) : ?>
								<?php while (have_rows('facts')) : the_row(); ?>
				    				<?php array_push($facts, get_sub_field('fact')); ?>
								<?php endwhile; ?>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>

					<script type='text/javascript'>
						<?php
						    $facts = json_encode($facts);
						    echo "var facts = ". $facts . ";\n";
						?>

						jQuery(document).ready(function( $ ) {
							$('.did-you-know .accordion-item .accordion-title, .shuffle').on('click', function () {
								if (!$(this).parent().hasClass('is-active')) {
								    var randomFact = facts[Math.floor(Math.random()*facts.length)];
								    $('.did-you-know .accordion-content .fact').html(randomFact);
								}
					   		});

					   		$('.shuffle').on('click', function () {
							    var randomFact = facts[Math.floor(Math.random()*facts.length)];
							    $('.did-you-know .accordion-content .fact').html(randomFact);
					   		});
				   		});
					</script>
					<div class="inner-footer">
						<div class="row">
							<div class="large-18 columns small-centered text-center">
								<img class="footer-logo" src="<?php echo get_field('logo', 'option')['sizes']['landscape_300x205']; ?>" alt="<?php echo get_field('logo', 'option')['alt']; ?>" width="<?php echo get_field('logo', 'option')['sizes']['landscape_300x205-width']; ?>" height="<?php echo get_field('logo', 'option')['sizes']['landscape_300x205-height']; ?>">
								
								<p class="copyright">
									© <?php echo date('Y'); ?> <?php bloginfo('name'); ?> . <a href="/privacy-policy" target="_blank">Privacy Policy</a> . <a href="/cookies" target="_blank">Cookies</a> . Web Design by <a target="_blank" rel="nofollow noopener" href="https://www.embryodigital.co.uk">Embryo</a>
								</p>

								<img class="footer-image" src="<?php echo get_field('footer_image', 'option')['sizes']['landscape_550x350']; ?>" width="<?php echo get_field('footer_image', 'option')['sizes']['landscape_550x350-width']; ?>" height="<?php echo get_field('footer_image', 'option')['sizes']['landscape_550x350-height']; ?>" alt="<?php echo get_field('footer_image', 'option')['alt']; ?>">
							</div>
	    				</div>
					</div> <!-- end #inner-footer -->
				</footer> <!-- end .footer -->
			</div>  <!-- end .off-canvas-content -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->