<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />	
		<?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<link rel="preload" href="<?php echo get_template_directory_uri()?>/assets/fonts/grandstander/ga6fawtA-GpSsTWrnNHPCSIMZhhKpFjyNZIQD7i5P3_csfNDiQhJYQ.woff" as="font" crossorigin="anonymous" />
		<link rel="preload" href="<?php echo get_template_directory_uri()?>/assets/fonts/grandstander/ga6fawtA-GpSsTWrnNHPCSIMZhhKpFjyNZIQD7i5P3DcsfNDiQhJYSuC.woff" as="font" crossorigin="anonymous" />
		<link rel="preload" href="<?php echo get_template_directory_uri()?>/assets/fonts/grandstander/ga6fawtA-GpSsTWrnNHPCSIMZhhKpFjyNZIQD7i5P3HcsfNDiQhJYSuC.woff" as="font" crossorigin="anonymous" />
		
		<link rel="preconnect" href="https://use.typekit.net">
		<link rel="preconnect" href="https://p.typekit.net">

		<link rel="preload" href="https://use.typekit.net/xbf7cge.css" as="style">
	  	<link rel="stylesheet" href="https://use.typekit.net/xbf7cge.css">

		<script>
			window.addEventListener('DOMContentLoaded', function() {
				setTimeout(function(){
				    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				    })(window,document,'script','dataLayer','GTM-WF7TW66');


				    !function(f,b,e,v,n,t,s)
				    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				    n.queue=[];t=b.createElement(e);t.async=!0;
				    t.src=v;s=b.getElementsByTagName(e)[0];
				    s.parentNode.insertBefore(t,s)}(window, document,'script',
				    'https://connect.facebook.net/en_US/fbevents.js');
				    fbq('init', '5169755799763150');
				    fbq('track', 'PageView');
				}, 3000); 
			});
		</script>

		<?php wp_head(); ?>

		<noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=5169755799763150&ev=PageView&noscript=1"/></noscript>

	</head>
			
	<body <?php body_class(); ?>>

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WF7TW66" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<div class="off-canvas-wrapper">
			
			<!-- Load off-canvas container. Feel free to remove if not using. -->			
			<?php get_template_part( 'parts/content', 'offcanvas' ); ?>
			
			<div class="off-canvas-content" data-off-canvas-content>
				
				<header class="header" role="banner">
							
					 <!-- This navs will be applied to the topbar, above all content 
						  To see additional nav styles, visit the /parts directory -->
					 <?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>
		
				</header> <!-- end .header -->