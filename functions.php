<?php
/** 
 * For more info: https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */			
	
// Theme support options
require_once(get_template_directory().'/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/functions/menu.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/functions/translation/translation.php');

// Add in the ACF fields
require_once(get_template_directory().'/acf/page-builder.php'); 

// Adds site styles to the WordPress editor
// require_once(get_template_directory().'/functions/editor-styles.php'); 

// Remove Emoji Support
// require_once(get_template_directory().'/functions/disable-emoji.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/functions/admin.php'); 

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/acf-json';
	return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
	unset($paths[0]);
	$paths[] = get_stylesheet_directory() . '/acf-json';
	return $paths;   
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
	'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
}

// Dequeue's some assets on pages where they're not needed
function dequeue_assets() {
    if (!is_page('test-page') && !is_page('vets') && !is_page('owners') && !is_page('retailers') && !is_page('rescue-centre') && !is_page('competition')) {
        wp_dequeue_script('contact-form-7');
        wp_dequeue_style('contact-form-7');
        wp_dequeue_script('wpcf7-recaptcha');
        wp_dequeue_script('google-recaptcha');
        wp_dequeue_style('wpcf7-redirect-script-frontend');
        wp_dequeue_script('wpcf7-redirect-script');
    }
}
add_action( 'wp_enqueue_scripts', 'dequeue_assets', 99 );

// Disables REFILL function in WPCF7 if Recaptcha is in use
add_action('wp_enqueue_scripts', 'wpcf7_recaptcha_no_refill', 15, 0);
function wpcf7_recaptcha_no_refill() {
	$service = WPCF7_RECAPTCHA::get_instance();
	if ( ! $service->is_active() ) {
		return;
	}
	wp_add_inline_script('contact-form-7', 'wpcf7.cached = 0;', 'before' );
}

function slugify($text) {
	$text = preg_replace('~[^\pL\d]+~u', '-', $text);
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	$text = preg_replace('~[^-\w]+~', '', $text);
	$text = trim($text, '-');
	$text = preg_replace('~-+~', '-', $text);
	$text = strtolower($text);

	if (empty($text)) {
		return 'n-a';
	}

	return $text;
}

// Adds colour palette to WYSIWYG
function custom_mce_colors($init) {

    $custom_colors = '
        "f6a300", "Orange",
        "454b1c", "Dark Green",
        "b9c407", "Light Green",
        "fad733", "Yellow",
        "000000", "Black",
        "343434", "Dark Gray",
        "424242", "Medium Gray",
        "efefef", "Light Gray",
        "FFFFFF", "White"
    ';

    $init['textcolor_map'] = '['.$custom_colors.']';
    $init['textcolor_rows'] = 2;

    return $init;
}
add_filter('tiny_mce_before_init', 'custom_mce_colors');

// Removes colour picker from WYSIWYG
function remove_color_picker_mce( $plugins ) {       

    foreach ( $plugins as $key => $plugin_name ) {
        if ( 'colorpicker' === $plugin_name ) {
            unset( $plugins[ $key ] );
            return $plugins;            
        }
    }

    return $plugins;            
}
add_filter( 'tiny_mce_plugins', 'remove_color_picker_mce' );

add_post_type_support( 'page', 'excerpt' );

add_filter('wpcf7_autop_or_not', '__return_false');

function pre_r($value) {
	echo "<pre>";
	print_r($value);
	echo "</pre>";
}