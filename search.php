<?php 
/**
 * The template for displaying search results pages
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 */
 	
get_header(); ?>
			
	<div class="content">

		<div class="inner-content">
	
			<main class="main" role="main">

				<div class="row "><div class="columns section-divider medium-divider">&nbsp;</div></div>

				<header>
					<div class="row">
						<div class="columns text-center">
							<h1 class="archive-title"><?php _e( 'Search Results for:', 'jointswp' ); ?> <?php echo esc_attr(get_search_query()); ?></h1>
						</div>
					</div>
				</header>

				<div class="category-posts">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				 
						<!-- To see additional archive styles, visit the /parts directory -->

						<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>
					    
					<?php endwhile; ?>	

						<div class="row">
							<div class="columns text-right">
								<?php joints_page_navi(); ?>
							</div>
						</div>
						
					<?php else : ?>
					
						<?php get_template_part( 'parts/content', 'missing' ); ?>
							
				    <?php endif; ?>
				</div>

				<div class="row "><div class="columns section-divider medium-divider">&nbsp;</div></div>
	
		    </main> <!-- end #main -->
		
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
