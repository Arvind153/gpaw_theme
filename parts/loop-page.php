<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
						
	<?php get_template_part('parts/loops/builder', 'loop'); ?>

	<?php 
		$parent_id = $post->post_parent;
		$post_parent_slug = get_post_field( 'post_name', $parent_id );

		if ($post->post_parent != 0 && $post_parent_slug == 'guinea-pig-welfare') : ?>
		<?php get_template_part('parts/blocks/block_welfare_subpages'); ?>
		<div class="row "><div class="columns section-divider medium-divider">&nbsp;</div></div>
	<?php endif; ?>
					
</article> <!-- end article -->