<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="top-bar-container" data-sticky-container>
	<div class="hide-for-xlarge callout">
		<div class="row">
			<div class="columns">
				<span class="event-date"><?php echo get_field('event_date', 'options'); ?></span>
			</div>
		</div>
	</div>
	<div class="sticky" data-sticky data-options="anchor: page; marginTop: 0; stickyOn: small;">
		<div class="row expanded">
			<div class="top-bar" id="top-bar-menu">
				<div class="top-bar-left float-left">
					<ul class="menu">
						<li class="logo">
							<a href="<?php echo home_url(); ?>" title="GPAW Homepage">
								<img class="first-logo" src="<?php echo get_field('logo', 'option')['sizes']['landscape_300x205']; ?>" width="<?php echo get_field('logo', 'option')['sizes']['landscape_300x205-width']; ?>" height="<?php echo get_field('logo', 'option')['sizes']['landscape_300x205-height']; ?>" alt="<?php echo get_sub_field('logo')['alt']; ?>">
								<img class="second-logo hide-for-small-only" src="<?php echo get_field('second_logo', 'option')['sizes']['landscape_300x81']; ?>" width="<?php echo get_field('second_logo', 'option')['sizes']['landscape_300x81-width']; ?>" height="<?php echo get_field('second_logo', 'option')['sizes']['landscape_300x81-height']; ?>" alt="<?php echo get_sub_field('second_logo')['alt']; ?>">
							</a>
						</li>
					</ul>
				</div>
				<div class="top-bar-right vertical-middle float-right">
					<div class="">
						<div class="row">
							<div class="event-notice show-for-xlarge">
								<div class="event-notice-container text-center">
									<span class="event-date">
										<?php echo get_field('event_date', 'options'); ?>

										<a href="<?php echo get_field('facebook_link', 'options')['url']; ?>" target="_blank" rel="nofollow noopener">
											<span>
												<?php 	
													$arrContextOptions=array(
													    "ssl"=>array(
													        "verify_peer"=>false,
													        "verify_peer_name"=>false,
													    ),
													);
													$svg_file = file_get_contents(get_template_directory_uri() . '/assets/images/icons/facebook-logo.svg', false, stream_context_create($arrContextOptions));
													$find_string   = '<svg';
													$position = strpos($svg_file, $find_string);
													$svg_file_new = substr($svg_file, $position);

													if($find_string) {
														echo $svg_file;
													}
												?>
											</span>
										</a>

										<a href="<?php echo get_field('instagram_link', 'options')['url']; ?>" target="_blank" rel="nofollow noopener">
											<span>
												<?php 	
													$arrContextOptions=array(
													    "ssl"=>array(
													        "verify_peer"=>false,
													        "verify_peer_name"=>false,
													    ),
													);
													$svg_file = file_get_contents(get_template_directory_uri() . '/assets/images/icons/instagram-logo.svg', false, stream_context_create($arrContextOptions));
													$find_string   = '<svg';
													$position = strpos($svg_file, $find_string);
													$svg_file_new = substr($svg_file, $position);
													if($find_string) {
														echo $svg_file;
													}
												?>
											</span>
										</a>
									</span>
								</div>
							</div>
							<ul class="menu float-right hide-for-xlarge">
								<li class="instagram-logo">
									<a href="<?php echo get_field('instagram_link', 'options')['url']; ?>" target="_blank" rel="nofollow noopener">
										<span>
											<?php 	
												$arrContextOptions=array(
												    "ssl"=>array(
												        "verify_peer"=>false,
												        "verify_peer_name"=>false,
												    ),
												);  
												$svg_file = file_get_contents(get_template_directory_uri() . '/assets/images/icons/instagram-logo.svg', false, stream_context_create($arrContextOptions));
												$find_string   = '<svg';
												$position = strpos($svg_file, $find_string);
												$svg_file_new = substr($svg_file, $position);
												if($find_string) {
													echo $svg_file;
												}
											?>
										</span>
									</a>
								</li>
								<li class="facebook-logo">
									<a href="<?php echo get_field('facebook_link', 'options')['url']; ?>" target="_blank" rel="nofollow noopener">
										<span>
											<?php 	
												$arrContextOptions=array(
												    "ssl"=>array(
												        "verify_peer"=>false,
												        "verify_peer_name"=>false,
												    ),
												);  

												$svg_file = file_get_contents(get_template_directory_uri() . '/assets/images/icons/facebook-logo.svg', false, stream_context_create($arrContextOptions));
												$find_string   = '<svg';
												$position = strpos($svg_file, $find_string);
												$svg_file_new = substr($svg_file, $position);

												echo $svg_file;
											?>
										</span>
									</a>
								</li>
								<li class="menu-toggler">
									<button data-toggle="off-canvas" title="open the menu">
										<span>
											<?php 	
												$arrContextOptions=array(
												    "ssl"=>array(
												        "verify_peer"=>false,
												        "verify_peer_name"=>false,
												    ),
												);
												$svg_file = file_get_contents(get_template_directory_uri() . '/assets/images/icons/menu.svg', false, stream_context_create($arrContextOptions));
												$find_string   = '<svg';
												$position = strpos($svg_file, $find_string);
												$svg_file_new = substr($svg_file, $position);

												echo $svg_file;
											?>
										</span>
									</button>
								</li>
							</ul>
						</div>
						<div class="row show-for-xlarge">
							<?php joints_top_nav(); ?>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>