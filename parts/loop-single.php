<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
						
	<?php get_template_part('parts/loops/builder', 'loop'); ?>
					
</article> <!-- end article -->