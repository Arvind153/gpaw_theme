<?php if(have_rows('buttons')) : ?>
	<?php while( have_rows('buttons')) : the_row(); ?>
		<?php 
			$button_colour = get_sub_field('colour');
			$button_style = get_sub_field('style');
			$button_type = get_sub_field('button_type');
			$button_target = get_sub_field('link')['target'] ? get_sub_field('link')['target'] : '_self';
			$button_url = get_sub_field('link')['url'] ? get_sub_field('link')['url'] : get_sub_field('file')['url'];
			$button_title = get_sub_field('link')['title'] ? get_sub_field('link')['title'] : get_sub_field('link')['text'];
			$button_text = get_sub_field('link')['text'] ? get_sub_field('link')['text'] : get_sub_field('link')['title'];
			$nofollow = true;
			if (strpos($button_url, get_site_url()) !== false) {
			    $nofollow = false;
			}
		?>
		<a href="<?php echo $button_url; ?>" <?php if($nofollow == true) : ?>rel="nofollow noopener"<?php endif; ?> title="<?php echo $button_title; ?>" target="<?php echo $button_target; ?>" class="button <?php echo $button_colour; ?> <?php echo $button_style; ?>">
			<?php echo $button_text; ?>
		</a>
	<?php endwhile; ?>
<?php endif; ?>