<?php
/**
 * The template part for displaying offcanvas content
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="off-canvas position-right" id="off-canvas" data-off-canvas>

	<?php joints_off_canvas_nav(); ?>

	<div class="logos hide-for-medium">
		<img class="second-logo" src="<?php echo get_field('second_logo', 'option')['sizes']['landscape_300x81']; ?>" width="<?php echo get_field('second_logo', 'option')['sizes']['landscape_300x81-width']; ?>" height="<?php echo get_field('second_logo', 'option')['sizes']['landscape_300x81-height']; ?>" alt="<?php echo get_sub_field('second_logo')['alt']; ?>">
	</div>
</div>
