<section class="block-content-events">
	<div class="row" data-equalizer>
		<div class="small-24 large-11 xlarge-12 columns <?php echo $content_class; ?> events vertical-align middle" data-equalizer-watch>
			<?php if( have_rows('events')) : ?>
			    <?php while( have_rows('events') ) : the_row(); ?>
			    	<?php if( have_rows('items')) : ?>
			    		<ul class="no-bullet event-list">
				    	    <?php while( have_rows('items') ) : the_row(); ?>
				        		<?php
				        			$title = get_sub_field('title');
				        			$date = get_sub_field('date');
				        			$link = get_sub_field('link');
				        			$link_url = $link['url'];
				        			$link_title = $link['title'];
				        			$link_target = $link['target'] ? $link['target'] : '_self';
				        		?>
				        		<li>
				        			<?php if( $link ) : ?>
			        					<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
			        				<?php endif; ?>
				        				<div class="content">
					        				<h6><?php echo $title; ?></h6>
					        				<span><?php echo $date; ?></span>
					        			</div>
				        				<div class="like-button">
				        					<img src="<?php echo get_field('event_like_button', 'options')['url']; ?>" alt="<?php echo get_field('event_like_button', 'options')['alt']; ?>">
				        				</div>
				        			<?php if( $link ) : ?>
				        				</a>
				        			<?php endif; ?>
				        		</li>
				            <?php endwhile; ?>
			            </ul>
			        <?php endif; ?>
			    <?php endwhile; ?>
			<?php endif; ?>
		</div>

		<div class="row hide-for-large"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>

		<div class="small-24 large-12 xlarge-10 columns <?php echo $content_class; ?> content vertical-align middle" data-equalizer-watch>
			<div class="content-inner">
				<?php if(have_rows('content')) : ?>
					<?php while(have_rows('content')) : the_row(); ?>
						<?php echo get_sub_field('content'); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php if(have_rows('button_group')) : ?>
					<?php while(have_rows('button_group')) : the_row(); ?>
						<div class="button-group stacked-for-small">
							<?php get_template_part('parts/global/button', 'group'); ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>