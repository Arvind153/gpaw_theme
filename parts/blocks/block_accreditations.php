<section class="block-accreditations">
	<div class="row columns">
		<?php if( have_rows('accreditation_groups') ): ?>
			<div class="large-22 xlarge-20 columns small-centered text-center accreditations-wrapper">
		    	<?php while( have_rows('accreditation_groups') ): the_row(); ?>
					<?php 
						$select_accreditations = get_sub_field('select_accreditations');

						if($select_accreditations == 'custom') {
							$accreditations = get_sub_field('custom_accreditations');
						} else {
							$accreditations = get_field('accreditations', 'options');
						}
					?>
		       		<?php if($accreditations) : ?>
		       			<div class="accreditations">
			       			<?php echo get_sub_field('content')['content']; ?>
				       	    <div class="row small-up-2 medium-up-3 large-up-5 align-center" data-equalizer>
				       	        <?php foreach( $accreditations as $accreditation ): ?>
				       				<div class="columns end vertical-align middle accreditation" data-equalizer-watch>
				       					<img class="accreditation-img" src="<?php echo esc_url($accreditation['sizes']['landscape_150x100']); ?>" width="<?php echo $accreditation['sizes']['landscape_150x100-width']; ?>" height="<?php echo $accreditation['sizes']['landscape_150x100-height']; ?>" alt="<?php echo esc_attr($accreditation['alt']); ?>" />
				       				</div>
				       	        <?php endforeach; ?>
				       	    </div>
			       	    </div>
	       	   		<?php endif; ?>
		    	<?php endwhile; ?>
		    </div>
		<?php endif; ?>
	</div>
</section>