<section class="block-content-form">
	<div class="row">
		<div class="small-centered text-center small-24 medium-24 large-22 xlarge-20 xxlarge-18 columns content">
			<div class="content-inner">
				<?php if(have_rows('content')) : ?>
					<?php while(have_rows('content')) : the_row(); ?>
						<?php echo get_sub_field('content'); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php if(have_rows('button_group')) : ?>
					<?php while(have_rows('button_group')) : the_row(); ?>
						<div class="button-group stacked-for-small">
							<?php get_template_part('parts/global/button', 'group'); ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>

		<div class="row"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>

		<div class="small-centered small-24 medium-24 large-22 xlarge-20 xxlarge-20 columns content">
			<?php if(have_rows('contact_form')) : ?>
				<?php while(have_rows('contact_form')) : the_row(); ?>
					<?php $contact_form = get_sub_field('contact_form'); ?>
					<?php if($contact_form) : ?>
						<div class="row columns">
							<?php echo do_shortcode('[contact-form-7 id="' . $contact_form->ID . '" title="' . $contact_form->post_title . '"]'); ?>
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>		
	</div>
</section>



