<?php
	$has_content = get_sub_field('content')['content'];
?>
<section class="block-products">
	<?php if($has_content) : ?>
		<div class="row">
			<div class="small-centered text-center small-24 medium-24 large-22 xlarge-20 xxlarge-20 columns content">
				<div class="content-inner">
					<?php if(have_rows('content')) : ?>
						<?php while(have_rows('content')) : the_row(); ?>
							<?php echo get_sub_field('content'); ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			
			<div class="row"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>
		</div>
	<?php endif; ?>

	<?php if(have_rows('products')) : ?>
		<div class="products-wrapper" data-equalizer="product-image" data-equalize-by-row="true">
		    <div class="row products small-up-2 large-up-4" data-equalizer="product-content" data-equalize-by-row="true">
			    <?php while(have_rows('products')) : the_row(); ?>
			        <?php
			        	$image = get_sub_field('image');
			        	$description = get_sub_field('description');
			        	$link = get_sub_field('link');
			        ?>
			        <div class="columns product end">
			            <div class="product-content" >
		            		<div class="row inner">
		            			<div class="small-22 medium-20 large-20 xlarge-18 columns small-centered text-center">
		            				<?php if($image) : ?> 
		            					<?php if($link) : ?>
		            						<div class="product-image vertical-align bottom" data-equalizer-watch="product-image">
		        								<a  title="<?php echo preg_replace( "/\r|\n/", "", strip_tags($description) ); ?> - Buy now" href="<?php echo $link['url']; ?>" target="<?php echo $link['target'] ? $link['target'] : '_self'; ?>">
			            							<img src="<?php echo $image['sizes']['medium']; ?>" width="<?php echo $image['sizes']['medium-width']; ?>" height="<?php echo $image['sizes']['medium-height']; ?>" alt="<?php echo $image['alt']; ?>">
			            						</a>
		            						</div>
		            					<?php endif; ?>
		            				<?php endif; ?>
		            				<div class="product-description" data-equalizer-watch="product-content">
		            					<?php if($description) : ?>
		            						<?php if($link) : ?>
		            							<a target="<?php echo $link['target'] ? $link['target'] : '_self'; ?>" href="<?php echo $link['url']; ?>">
		            								<?php echo $description; ?>
		            							</a>
		            						<?php endif; ?>
		            					<?php endif; ?>
		            				</div>
		            				<?php if($link) : ?>
		            					<div class="button-group expanded">
		            						<a title="<?php echo preg_replace( "/\r|\n/", "", strip_tags($description) ); ?> - Buy now" class="button orange" target="<?php echo $link['target'] ? $link['target'] : '_self'; ?>" href="<?php echo $link['url']; ?>">
		            							<?php echo $link['title']; ?>		
		            						</a>
		            					</div>
		            				<?php endif; ?>
		            			</div>
		            		</div>
			            </div>
			        </div>
			    <?php $count++; endwhile; ?>
		    </div>
	    </div>
	<?php endif; ?>
</section>