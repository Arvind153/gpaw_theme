<?php
    $title = get_sub_field('title');
    $welfare_needs = get_sub_field('welfare_needs');
?>
<style>
    .welfare-needs_list_wrapper {
        filter: drop-shadow(7px 8px #545454);
    }
    .welfare-needs_list {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: space-between;
        background: white;
        gap: 1rem;
        margin: 1rem auto;
        max-width: 1000px;
        color: black;
        padding: 1rem;
    }
    
    .welfare-needs_list_wrapper:nth-child(odd) .welfare-needs_list {
        clip-path: polygon(1% 3%, 100% 3%, 99% 100%, 0% 100%);
    }
    
    .welfare-needs_list_wrapper:nth-child(even) .welfare-needs_list {
        clip-path: polygon(0% 0%, 99% 2%, 100% 95%, 1% 99%);
    }
    
    .welfare-needs_list_wrapper:nth-child(3) .welfare-needs_list {
        clip-path: polygon(0% 2%, 100% 0%, 100% 100%, 0.5% 95%);
    }

    .welfare-needs-section h2 {
        text-align: center;
        padding: 1rem 0;
    }

    .icon_left {
        width: 200px;
    }

    .welfare-needs_text {
        flex: 1;
        margin-bottom: unset;
    }

    .welfare-needs-section {
        padding: 2rem 0;
    }

    @media (max-width: 800px) {
        .welfare-needs_list {
            flex-direction: column;
            align-items: flex-start;
        }
    }

</style>

<section class="welfare-needs-section">
    <h2><?= $title ?></h2>
    <div class="welfare-needs_container">
    <?php foreach($welfare_needs as $welfare_need) { 
        $icon_left = $welfare_need['icon_left'];
        $text = $welfare_need['text']; 
        $icon_right = $welfare_need['icon_right'];
        ?>
        <div class="welfare-needs_list_wrapper">
            <div class="welfare-needs_list">
                <img class="icon_left" src="<?= wp_get_attachment_image_src($icon_left, 'medium')[0] ?>" alt="">
                <h3 class="welfare-needs_text"><?= $text ?></h3>
                <img src="<?= wp_get_attachment_image_src($icon_right)[0] ?>" alt="">
            </div>
        </div>
    <?php } ?>
    </div>
</section>