<?php
	$layout = get_sub_field('layout');	
	$centralise_content = get_sub_field('centralise_content');
	$css_classes = array();

	if($centralise_content == true) { 
		$css_classes[] = 'small-centered text-center ';
	}

	switch ($layout) {
	    case 'width-100':
	        $css_classes[] = '';
	        break;
	    case 'width-75':
	        $css_classes[] = 'small-24 medium-24 large-22 xlarge-20 xxlarge-20';
	        break;
	    case 'width-50':
	        $css_classes[] = 'small-uncentered small-24 medium-24 large-12 xlarge-11 xxlarge-11';
	        break;
	}
?>

<section class="block-two-column-content">
	<div class="row">
		<?php if(have_rows('content_left')) : ?>
			<?php while(have_rows('content_left')) : the_row(); ?>
				<div class="<?php foreach($css_classes as $class){ echo $class; } ?> columns content">
					<div class="content-inner">
						<?php echo get_sub_field('content'); ?>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php if($layout == 'width-50') : ?>
			<?php if(have_rows('content_right')) : ?>
				<?php while(have_rows('content_right')) : the_row(); ?>
					<div class="<?php foreach($css_classes as $class){ echo $class; } ?> columns content">
						<div class="content-inner">
							<?php echo get_sub_field('content'); ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>	
		<?php endif; ?>	
		
		<?php if(have_rows('button_group')) : ?>
			<?php while(have_rows('button_group')) : the_row(); ?>
				<div class="button-group stacked-for-small align-center">
					<?php get_template_part('parts/global/button', 'group'); ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>