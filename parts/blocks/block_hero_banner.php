<?php 
	$background = get_sub_field('background_image');
	$background_small = $background['sizes']['square_650x650'];
	$background_medium = $background['sizes']['square_1024x1024'];
	$background_large = $background['sizes']['landscape_1200x1024'];
	$background_xlarge = $background['sizes']['landscape_1440x1080'];
	$background_xxlarge = $background['sizes']['landscape_1920x1080'];
?>
<header class="block-hero-banner" data-interchange="[<?php echo $background_small; ?>, small], [<?php echo $background_medium; ?>, medium], [<?php echo $background_large; ?>, large], [<?php echo $background_xlarge; ?>, xlarge], [<?php echo $background_xxlarge; ?>, xxlarge]">
	<div class="overlay vertical-align middle">
		<div class="hero-banner-inner">
			<section>
				<div class="row">
					<div class="large-18 xlarge-16 xxlarge-14 columns">
						<?php if(have_rows('content')) : ?>
							<?php while(have_rows('content')) : the_row(); ?>
								<?php echo get_sub_field('content'); ?>
							<?php endwhile; ?>
						<?php endif; ?>
						<?php if(have_rows('button_group')) : ?>
							<?php while(have_rows('button_group')) : the_row(); ?>
								<div class="button-group stacked-for-small">
									<?php get_template_part('parts/global/button', 'group'); ?>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</section>
		</div>
	</div>
</header>