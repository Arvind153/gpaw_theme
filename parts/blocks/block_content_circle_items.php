<?php
	$simple_items = get_sub_field('item_settings')['simple_items'];
	$overlap_section_below = get_sub_field('item_settings')['overlap_section_below'];
	$has_content = get_sub_field('content')['content'];
?>

<section class="block-content-page-links <?php if($overlap_section_below == true) { echo 'overlap-section-below'; } ?>">
	<div class="row" data-equalizer="outer-content">
		<?php if($has_content) : ?>
			<div class="small-centered text-center small-24 medium-24 large-22 xlarge-20 xxlarge-20 columns content">
				<div class="content-inner">
					<?php if(have_rows('content')) : ?>
						<?php while(have_rows('content')) : the_row(); ?>
							<?php echo get_sub_field('content'); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php if(have_rows('button_group')) : ?>
						<?php while(have_rows('button_group')) : the_row(); ?>
							<div class="button-group stacked-for-small">
								<?php get_template_part('parts/global/button', 'group'); ?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			
			<div class="row"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>
		<?php endif; ?>

		<?php if( have_rows('item_group') ): ?>
			<?php while( have_rows('item_group') ): the_row();  ?>
				<div class="columns page-links">
					<?php if( have_rows('items') ): ?>
						<div class="small-up-2 medium-up-2 large-up-3 xlarge-up-5 xxlarge-up-5 links" data-equalizer="inner-content">
							<?php while( have_rows('items') ): the_row();  ?>

								<?php
									$content = get_sub_field('content');
									$content_type = get_sub_field('content_type');
								?>

								<div class="columns link <?php echo $number; ?>" data-equalizer-watch="inner-content">
									<div class="inner text-center normal">
										<?php if($content_type == 'custom') : ?>
											<?php $custom = get_sub_field('custom'); ?>
											
											<?php if($custom['link']) : ?>

												<?php if($custom['link']) : ?>
													<a href="<?php echo $custom['link']['url']; ?>" title="<?php echo $custom['link']['title']; ?>">
												<?php endif; ?>
													<div class="circle">
														<div class="image">
															<?php 

																$extensions = array("svg");
																$file_ext = strtolower(end(explode('.', $custom['icon']['url'])));
																if(in_array($file_ext, $extensions) === true){
																	$arrContextOptions=array(
																	    "ssl"=>array(
																	        "verify_peer"=>false,
																	        "verify_peer_name"=>false,
																	    ),
																	);  

																	$svg_file = file_get_contents($custom['icon']['url'], false, stream_context_create($arrContextOptions));
																	$find_string   = '<svg';
																	$position = strpos($svg_file, $find_string);
																	$svg_file_new = substr($svg_file, $position);

																	echo $svg_file;
																} 
															?>
														</div>
													</div>
												<?php if($custom['link']) : ?>
													</a>
												<?php endif; ?>
											<?php endif; ?>	

											<?php if($custom['content'] && $simple_items == false) : ?>
												<?php echo $custom['content']; ?>
											<?php endif; ?>

											<?php if($custom['link'] && $simple_items == false) : ?>
												<div class="button-group align-center">
													<a class="button orange" href="<?php echo $custom['link']['url']; ?>" title="<?php echo $custom['link']['title']; ?>">
														<?php echo $custom['link']['title']; ?>
													</a>
												</div>
											<?php endif; ?>
										<?php elseif($content_type == 'page_post') : ?>
											<a href="<?php echo get_the_permalink(get_sub_field('pagepost')->ID); ?>" title="<?php echo get_sub_field('pagepost')->post_title; ?>">
												<div class="circle">
													<div class="image">
														<?php 

															$icon = get_field('icon', get_sub_field('pagepost')->ID);

															$extensions = array("svg");
															$file_ext = strtolower(end(explode('.', $icon['url'])));
															if(in_array($file_ext, $extensions) === true){
																$arrContextOptions=array(
																    "ssl"=>array(
																        "verify_peer"=>false,
																        "verify_peer_name"=>false,
																    ),
																);  

																$svg_file = file_get_contents($icon['url'], false, stream_context_create($arrContextOptions));
																$find_string   = '<svg';
																$position = strpos($svg_file, $find_string);
																$svg_file_new = substr($svg_file, $position);

																echo $svg_file;
															} 
														?>
													</div>
												</div>
											</a>
											
											<a href="<?php the_permalink(get_sub_field('pagepost')->ID); ?>" title="<?php echo get_sub_field('pagepost')->post_title; ?>"><h6><?php echo get_sub_field('pagepost')->post_title; ?></h6></a>

											<?php if($simple_items == false) : ?>
												<p><?php echo get_sub_field('pagepost')->post_excerpt; ?></p>
												<div class="button-group align-center">
													<a class="button orange" href="<?php the_permalink(get_sub_field('pagepost')->ID); ?>" title="<?php echo get_sub_field('pagepost')->post_title; ?>">
														Read more
													</a>
												</div>
											<?php endif; ?>
										<?php endif; ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>