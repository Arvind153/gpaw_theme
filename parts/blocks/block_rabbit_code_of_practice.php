<?php 
    $title = get_sub_field('title');
    $letter_text = get_sub_field('letter_text');
    $description = get_sub_field('description');
    $link = get_sub_field('link');
?>

<style>
    .rabbit-code-of-practice {
        display: flex;
        flex-wrap: wrap;
        flex-direction: row-reverse;
        gap: 3rem;
        align-items: center;
        max-width: 1000px;
        margin: 0 auto;
        justify-content: center;
    }

    .letter-wrapper {
        padding: 2rem;
        padding-top: unset;
        background-color: #fff;
        color: #000;
        box-shadow: 5px 5px #545454;
        position: relative;
        z-index: 2;
    }

    .letter h4 {
        font-size: 0.7rem;
        margin-bottom: 1em;
        color: #235c2b;
    }

    .letter p {
        font-size: 0.5rem;
        margin-bottom: unset;
    }

    .letter li {
        font-size: 0.5rem;
    }

    .letter span {
        font-size: 0.4rem;
    }

    .letter a {
        font-size: 0.5rem;
        color: blue !important;
    }

    .code-of-practice_text {
        flex: 1 1 500px;
        display: flex;
        flex-direction: column;
    }

    .code-of-practice_text h2 {
        text-align: center;
        color: white;
    }

    .code-of-practice_text p {
        font-size: 1.5rem;
        color: white;
        text-align: center;
    }

    .code-of-practice_text a {
        margin: 0 auto;
    }

    .parent-letter-box {
        flex: 0 0 400px;
        position: relative;
    }

    .parent-letter-box .letter-icons {
        position: absolute;
        width: 200px;
    }

    .parent-letter-box .letter-icons.binky {
        left: -130px;
        top: 50%;
        transform: translateY(-50%);
    }

    .parent-letter-box .letter-icons.hayley {
        right: -100px;
        bottom: -50px;
        z-index: 2;
    }

    .parent-letter-box .letter-icons.speech-bubble {
        right: 0px;
        top: 85px;
        z-index: 2;
        width: 250px;
    }


</style>

<section class="rabbit-code-of-practice">
    <div class="code-of-practice_text">
        <h2><?= $title ?></h2>
        <p><?= $description ?></p>
        <a class="button white fill" style="color:#f6a300;" href="<?= $link['url'] ?>"><?= $link['title'] ?></a>
    </div>
    <div class="parent-letter-box">
        <div class="letter-wrapper"> 
            <div class="letter"><?= $letter_text; ?></div>
        </div>
        <img class="letter-icons hayley" src="<?= bloginfo('template_directory') . '/assets/images/Hayley.png' ?>" alt="Hayley">
        <img class="letter-icons binky" src="<?= bloginfo('template_directory') . '/assets/images/Binky.png' ?>" alt="Binky">
    </div>
</section>