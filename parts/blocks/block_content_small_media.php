<?php 
	$media = get_sub_field('media');
	$media_type = $media['media_type'];
	$box_shadow = $media['box_shadow'];
	$border_radius = $media['border_radius'];
	$layout = get_sub_field('layout'); 
	
	if($layout == 'content_left_media_right') {
		$image_class = 'large-push-18 xlarge-push-17';
		$content_class = 'large-pull-7 xlarge-pull-8';
	} else {
		$image_class = '';
	}
?>

<section class="block-content-media">
	<div class="row" data-equalizer data-equalize-on="medium">
		<div class="small-24 medium-8 large-6 xlarge-7 columns <?php echo $image_class; ?> media <?php echo $media_type; ?> vertical-align middle" data-equalizer-watch>
			<?php if($media_type == 'image') : ?>
				<div class="image-inner text-center <?php if($box_shadow == true) : ?>box-shadow<?php endif; ?> <?php if($border_radius == true) : ?>border-radius<?php endif; ?>">
					<?php 
						$image_size = $media['image_size'];
						$image_url = $media['image']['sizes'][$image_size];
						$image_width = $media['image']['sizes'][$image_size . '-width'];
						$image_height = $media['image']['sizes'][$image_size . '-height'];
						$image_alt = $media['image']['alt'];
					?>
					<img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>">
				</div>
			<?php elseif($media_type == 'video') : ?>
				<?php 
					$video = $media['video'];
					$video_placeholder_url = $media['video_placeholder']['sizes']['square_480x480'];
					$video_placeholder_width = $media['video_placeholder']['sizes']['square_480x480-width'];
					$video_placeholder_height = $media['video_placeholder']['sizes']['square_480x480-height'];
					$modal_id = uniqid();
				?>
				<div class="video-inner <?php if($box_shadow == true) : ?>box-shadow<?php endif; ?> <?php if($border_radius == true) : ?>border-radius<?php endif; ?>">
					<div class="video-background" style="background-image: url(<?php echo $video_placeholder_url; ?>);">
						<?php if (strpos(strtolower($video), 'youtube') !== false) : ?>
							<div class="video-controls">
								<button data-open="video-<?php echo $modal_id; ?>">
									<img src="<?php echo get_field('play_button', 'options')['url']; ?>" alt="play button">
								</button>
							</div>

							<div class="reveal large <?php echo $media_type; ?>" id="video-<?php echo $modal_id; ?>" data-reveal data-reset-on-close="true">
								<iframe width="560" height="315" src="<?php echo str_replace('https://www.youtube.com/watch?v=', '//www.youtube.com/embed/', $video); ?>" frameborder="0"></iframe>
								<button class="close-button" data-close aria-label="Close modal" type="button">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>

		<div class="row hide-for-medium"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>

		<div class="small-24 medium-15 large-17 xlarge-16 columns <?php echo $content_class; ?> content vertical-align middle" data-equalizer-watch>
			<div class="content-inner">
				<?php if(have_rows('content')) : ?>
					<?php while(have_rows('content')) : the_row(); ?>
						<?php echo get_sub_field('content'); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php if(have_rows('button_group')) : ?>
					<?php while(have_rows('button_group')) : the_row(); ?>
						<div class="button-group stacked-for-small">
							<?php get_template_part('parts/global/button', 'group'); ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>