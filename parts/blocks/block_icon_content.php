<?php
	$small = 'small-' . (24 / get_sub_field('items_per_row')['small']);
	$medium = 'medium-' . (24 / get_sub_field('items_per_row')['medium']);
	$large = 'large-' . (24 / get_sub_field('items_per_row')['large']);
	$xlarge = 'xlarge-' . (24 / get_sub_field('items_per_row')['xlarge']);
	$xxlarge = 'xxlarge-' . (24 / get_sub_field('items_per_row')['xxlarge']);

	$items = count(get_sub_field('items'));

	if($items % 2 == 0) {
		$total = 'even';
	} else {
		$total = 'odd';
	}
?>

<?php if(have_rows('items')) : ?>
	<section class="block-icon-content">
	    <div class="row icons total-<?php echo $total; ?>" data-equalizer data-equalize-by-row="true">
		    <?php $count = 1; while(have_rows('items')) : the_row(); ?>
		        <?php
		        	$icon = get_sub_field('icon');
		        	$content = get_sub_field('content');
		        	if(get_sub_field('full_width') == true) {
		        		$full_width = "full-width";
		        	}
		        	if($count % 2 == 0) {
		        		$number = 'even';
	        		} else {
	        			$number = 'odd';
	        		}
		        ?>
		        <div class="<?php echo $small; ?> <?php echo $medium; ?> <?php echo $large; ?> <?php echo $xlarge; ?> <?php echo $xxlarge; ?> columns icon end <?php echo $full_width, ' ', $number; ?> ">
		            <div class="icon-content" data-equalizer-watch>
		            	<div class="inner">
		            		<?php if($icon) : ?> 
		            			<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
		            		<?php endif; ?>

		            		<?php if($content) : ?>
		            			<?php echo $content; ?>
		            		<?php endif; ?>
		            	</div>
		            </div>
		        </div>
		    <?php $count++; endwhile; ?>
	    </div>
	</section>
<?php endif; ?>