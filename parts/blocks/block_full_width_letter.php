<?php 
    $title = get_sub_field('title');
    $letter_text = get_sub_field('letter_text');
    $description = get_sub_field('description');
    $link = get_sub_field('link');
    $buttons = get_sub_field('buttons');
?>

<style>

    .full-width-letter {
        max-width: 1000px;
        margin: 0 auto;
    }

    .letter-description {
        max-width: 750px;
        margin: 0 auto;
        margin-bottom: 2rem;
    }

    .letter-description p {
        font-size: 1.5rem;
        margin-bottom: 2rem;
    }

    .full-width-letter .letter-container {
        background: white;
        color: black;
        padding: 2rem 5rem;
        max-width: 750px;
        margin: 0 auto;
        box-shadow: 7px 7px #545454;
        position: relative;
        z-index: 1;
    }

    .parent-letter-box {
        position: relative;
    }

    .full-width-letter .letter-container h4 {
        font-size: .8rem;
        margin-bottom: .8rem;
        color: #235c2b;
    }

    .full-width-letter .letter-container h5 {
        font-size: 0.6rem;
        margin-bottom: 0.6rem;
    }

    .full-width-letter .letter-container p {
        font-size: 0.6rem;
        margin-bottom: unset;
    }
    .full-width-letter .letter-container li {
        font-size: 0.6rem;
    }
    .full-width-letter .letter-container a {
        font-size: 0.6rem;
        color: blue !important;
    }
    
    .parent-letter-box .letter-icons {
        position: absolute;
        width: 400px;
    }

    .parent-letter-box .letter-icons.binky {
        left: -150px;
        top: 50%;
        transform: translateY(-50%);
    }

    .parent-letter-box .letter-icons.hayley {
        right: -100px;
        bottom: -100px;
        z-index: 2;
    }

    .parent-letter-box .letter-icons.speech-bubble {
        right: 0px;
        top: 135px;
        z-index: 2;
        width: 250px;
    }

    .letter-buttons-container {
        display: flex;
        gap: 20px;
        flex-wrap: wrap;
    }

    .letter-button {
        flex-basis: 300px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    @media (max-width: 1000px) {
        .parent-letter-box .letter-icons.speech-bubble, 
        .parent-letter-box .letter-icons.binky,
        .parent-letter-box .letter-icons.hayley
        {
            display: none;
        }
    }


</style>

<section class="full-width-letter">
    <div class="letter-description">
        <p><?= $description ?></p>
        <div class="letter-buttons-container">
        <?php
            foreach($buttons as $button) { ?>
                <a class="letter-button button" style="background:#fad733;" target="<?= $button['button']['target'] ?>" title="<?= $button['button']['title'] ?>" href="<?= $button['button']['url'] ?>"><?= $button['button']['title'] ?></a>
            <?php
            }
        ?>
        </div>
    </div>
    <div class="parent-letter-box">
        <div class="letter-container">
            <div><?= $letter_text ?></div>
        </div>
        <img class="letter-icons hayley" src="<?= bloginfo('template_directory') . '/assets/images/Hayley.png' ?>" alt="Hayley">
        <img class="letter-icons binky" src="<?= bloginfo('template_directory') . '/assets/images/Binky.png' ?>" alt="Binky">
        <img class="letter-icons speech-bubble" src="<?= bloginfo('template_directory') . '/assets/images/Speech-Bubble.png' ?>" alt="Speech Bubble">
    </div>

</section>