<?php
	$has_content = get_sub_field('content')['content'];
?>

<section class="block-content-page-links">
	<div class="row" data-equalizer="outer-content">
		<?php if($has_content) : ?>
			<div class="small-centered text-center small-24 medium-24 large-22 xlarge-20 xxlarge-20 columns content">
				<div class="content-inner">
					<?php if(have_rows('content')) : ?>
						<?php while(have_rows('content')) : the_row(); ?>
							<?php echo get_sub_field('content'); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php if(have_rows('button_group')) : ?>
						<?php while(have_rows('button_group')) : the_row(); ?>
							<div class="button-group stacked-for-small">
								<?php get_template_part('parts/global/button', 'group'); ?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			
			<div class="row"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>
		<?php endif; ?>

		<?php if( have_rows('item_group') ): ?>
			<?php while( have_rows('item_group') ): the_row();  ?>
				<div class="columns page-links">
					<?php if( have_rows('items') ): ?>
						<div class="small-up-1 medium-up-1 large-up-2 xlarge-up-2 xxlarge-up-2 links" data-equalizer="inner-content">
							<?php while( have_rows('items') ): the_row();  ?>
								<?php $content_type = get_sub_field('content_type'); ?>
								<div class="columns link" data-equalizer-watch="inner-content">
									<div class="inner large-image">
										<?php if($content_type == 'custom') : ?>
											<?php
												$custom = get_sub_field('custom');
												$content = $custom['content'];
												$custom_image = $custom['image'];
											?>

											<img src="<?php echo $custom_image['sizes']['landscape_550x350']; ?>" alt="<?php echo $custom_image['alt']; ?>">
											<?php if($content) : ?>
												<?php echo $content; ?>
											<?php endif; ?>
											<?php if($custom['link']) : ?>
												<div class="button-group">
													<a class="button" href="<?php echo $custom['link']['url']; ?>">
														<?php echo $custom['link']['title']; ?>
													</a>
												</div>
											<?php endif; ?>
										<?php elseif($content_type == 'page_post') : ?>
						        			<?php
						        				$image_id = get_post_thumbnail_id(get_sub_field('pagepost')->ID);
						        				$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);
						        				$image_url = get_the_post_thumbnail_url(get_sub_field('pagepost')->ID, 'landscape_550x350');
						        				$image_attributes = wp_get_attachment_metadata($image_id);
						        				
						        				if($image_attributes) {
						        					$image_width = $image_attributes['sizes']['landscape_550x350']['width'];
						        					$image_height = $image_attributes['sizes']['landscape_550x350']['height'];
						        				}
						        			?>
							        		<?php if($image_url) : ?>
						        				<img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>">
							        		<?php endif; ?>

											<h6><?php echo get_sub_field('pagepost')->post_title; ?></h6>
											<p><?php echo get_sub_field('pagepost')->post_excerpt; ?></p>
											<div class="button-group align-center">
												<a class="button" title="Learn more about <?php echo get_sub_field('pagepost')->post_title; ?>" href="<?php the_permalink(get_sub_field('pagepost')->ID); ?>">Learn more</a>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>