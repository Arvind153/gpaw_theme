<?php 
	$visibility = get_sub_field('visibility'); 
	$size = get_sub_field('size');
	$image = get_sub_field('image');
	$link = get_sub_field('link');
	if($link) {
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_text = $link['text'];
		$link_text = $link['text'];
		$link_title = $link['title'] ? $link['title'] : $link['text'];
		$link_text = $link['text'] ? $link['text'] : $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
	}
	$vertical_position = get_sub_field('image_settings')['vertical_position'];
	$horizontal_position = get_sub_field('image_settings')['horizontal_position'];
	$overflow = get_sub_field('image_settings')['overflow'];
	$margin_top = get_sub_field('image_settings')['margin_top'];
	$margin_bottom = get_sub_field('image_settings')['margin_bottom'];
?>
<div class="row <?php if( $visibility ) { echo implode( ' ', $visibility ); } ?>">
	<div class="columns image-divider <?php if($margin_top) { echo $margin_top; } ?> <?php if($margin_bottom) { echo $margin_bottom; } ?> <?php echo $overflow; ?> <?php echo $size; ?>-divider <?php echo $vertical_position . ' ' . $horizontal_position; ?>">
		<?php if( $image ) : ?>
			<?php if( $link ) : ?>
				<a href="<?php echo esc_url( $link_url ); ?>" title="<?php echo $link_title; ?>" target="<?php echo esc_attr( $link_target ); ?>">
			<?php endif; ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
			<?php if( $link ) : ?>
				</a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>