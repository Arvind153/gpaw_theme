<?php
	$has_content = get_sub_field('content')['content'];
?>

<section class="block-content-page-links">
	<div class="row">
		<?php if($has_content) : ?>
			<div class="small-centered text-center small-24 medium-24 large-22 xlarge-20 xxlarge-20 columns content">
				<div class="content-inner">
					<?php if(have_rows('content')) : ?>
						<?php while(have_rows('content')) : the_row(); ?>
							<?php echo get_sub_field('content'); ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<?php if(have_rows('button_group')) : ?>
						<?php while(have_rows('button_group')) : the_row(); ?>
							<div class="button-group stacked-for-small">
								<?php get_template_part('parts/global/button', 'group'); ?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			
			<div class="row"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>
		<?php endif; ?>

		<?php if( have_rows('item_group') ): ?>
			<?php while( have_rows('item_group') ): the_row();  ?>
				<div class="columns page-links">
					<?php if( have_rows('items') ): ?>
						<div class="small-up-1 medium-up-2 large-up-2 xlarge-up-4 xxlarge-up-4 links" data-equalizer="inner-content">
							<?php while( have_rows('items') ): the_row();  ?>

								<?php $content_type = get_sub_field('content_type'); ?>

								<div class="columns link <?php echo $number; ?>" data-equalizer-watch="inner-content">
									<?php $background_colour = get_sub_field('background_colour'); ?>

									<div class="inner text-center background-color <?php echo $background_colour; ?>">
										<?php if($content_type == 'custom') : ?>
											<?php $custom_content = get_sub_field('custom_content'); ?>
											<?php echo $custom_content['content']; ?>
											<?php if($custom_content['link']) : ?>
												<div class="button-group align-center">
													<a class="button hollow white" title="<?php echo $custom_content['link']['title']; ?>"> href="<?php echo $custom_content['link']['url']; ?>">
														<?php echo $custom_content['link']['text']; ?>
													</a>
												</div>
											<?php endif; ?>
										<?php elseif($content_type == 'page_post') : ?>
											<h6><?php echo get_sub_field('pagepost')->post_title; ?></h6>
											<p><?php echo get_sub_field('pagepost')->post_excerpt; ?></p>
											<div class="button-group align-center">
												<a class="button hollow white" title="Learn more about <?php echo get_sub_field('pagepost')->post_title; ?>" href="<?php the_permalink(get_sub_field('pagepost')->ID); ?>">Learn more</a>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>