<?php 
	$visibility = get_sub_field('visibility'); 
	$size = get_sub_field('size');
?>
<div class="row <?php if( $visibility ) { echo implode( ' ', $visibility ); } ?>"><div class="columns section-divider <?php echo $size; ?>-divider">&nbsp;</div></div>