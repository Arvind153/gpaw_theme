<?php 
	$media = get_sub_field('media');
	$media_type = $media['media_type'];
	$box_shadow = $media['box_shadow'];
	$border_radius = $media['border_radius'];
?>

<section class="block-full-width-media">
	<div class="row">
		<div class="medium-22 large-20 xlarge-20 columns small-centered media <?php echo $media_type; ?>">
			<?php if($media_type == 'image') : ?>
				<div class="image-inner text-center <?php if($box_shadow == true) : ?>box-shadow<?php endif; ?> <?php if($border_radius == true) : ?>border-radius<?php endif; ?>">
					<?php 
						$image_size = 'landscape_1200x400';
						$image_url = $media['image']['sizes'][$image_size];
						$image_width = $media['image']['sizes'][$image_size . '-width'];
						$image_height = $media['image']['sizes'][$image_size . '-height'];
						$image_alt = $media['image']['alt'];
					?>
					<img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>">
				</div>
			<?php elseif($media_type == 'video') : ?>
				<?php 
					$video = $media['video'];
					$video_placeholder_url = $media['video_placeholder']['sizes']['landscape_1200x400'];
					$video_placeholder_width = $media['video_placeholder']['sizes']['landscape_1200x400-width'];
					$video_placeholder_height = $media['video_placeholder']['sizes']['landscape_1200x400-height'];
					$modal_id = uniqid();
				?>
				<div class="video-inner <?php if($box_shadow == true) : ?>box-shadow<?php endif; ?> <?php if($border_radius == true) : ?>border-radius<?php endif; ?>">
					<div class="video-background" style="background-image: url(<?php echo $video_placeholder_url; ?>);">
						<?php if (strpos(strtolower($video), 'youtube') !== false) : ?>
							<div class="video-controls">
								<button data-open="video-<?php echo $modal_id; ?>">
									<img src="<?php echo get_field('play_button', 'options')['url']; ?>" alt="play button">
								</button>
							</div>

							<div class="reveal large <?php echo $media_type; ?>" id="video-<?php echo $modal_id; ?>" data-reveal data-reset-on-close="true">
								<iframe width="560" height="315" src="<?php echo str_replace('https://www.youtube.com/watch?v=', '//www.youtube.com/embed/', $video); ?>" frameborder="0"></iframe>
								<button class="close-button" data-close aria-label="Close modal" type="button">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>