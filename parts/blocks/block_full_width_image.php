<?php 
    $imageID = get_sub_field('image');
?>

<?php
function raag_get_full_width_picture_tag($ID, $class='') {
  
  $xxlarge = wp_get_attachment_image_src($ID, '2048x2048')[0];
  $xlarge = wp_get_attachment_image_src($ID, '1536x1536')[0];
  $large = wp_get_attachment_image_src($ID, 'large')[0];
//   $medium_large = wp_get_attachment_image_src($ID, 'medium_large')[0];
//   $medium = wp_get_attachment_image_src($ID, 'medium')[0];
  $alt = wp_prepare_attachment_for_js($ID)['alt'];

  $style = "width:100%;object-fit:cover;";

  $picture = '<picture class="">
      <source srcset="%s" media="(max-width: 2000px)">
      <source srcset="%s" media="(max-width: 1500px)">
      <source srcset="%s" media="(max-width: 1000px)">
      <source srcset="%s" media="(max-width: 500px)">
      <img style="%s" class="%s" src="%s" alt="%s" />
    </picture>';

  return sprintf( $picture, $xlarge, $xxlarge, $xlarge, $large, $style, $class, $large, $alt);
}
?>

<style>
    .full_width_image img {
        position: relative;
        top: -3.6vw;
        margin-bottom: -3.6vw;
    }
</style>

<section class="full_width_image featured-image">
 <?= raag_get_full_width_picture_tag($imageID) ?>
</section>