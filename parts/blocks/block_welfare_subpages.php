<section class="block-welfare-subpages">
	<div class="row">
		<div class="small-centered text-center small-24 medium-24 large-22 xlarge-20 xxlarge-20 columns content">
			<h2>Have a look at our other welfare needs...</h2>

			<?php
				$args = array(
				    'post_type'      => 'page',
				    'posts_per_page' => -1,
				    'post_parent'    => $post->post_parent,
				    'order'          => 'ASC',
				    'orderby'        => 'menu_order',
				    'post__not_in' => array($post->ID),
				);

				$parent = new WP_Query( $args );
			?>

			<?php if ( $parent->have_posts() ) : ?>
				<div class="row small-up-2 medium-up-4 sub-pages">
				    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
				        <div class="columns text-center sub-page">
				            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				            	<div class="circle">
					            	<div class="image">
					            		<?php 
					            			$icon = get_field('icon');
					            			$extensions = array("svg");
					            			$file_ext = strtolower(end(explode('.', $icon['url'])));
					            			if(in_array($file_ext, $extensions) === true){
					            				$arrContextOptions=array(
					            				    "ssl"=>array(
					            				        "verify_peer"=>false,
					            				        "verify_peer_name"=>false,
					            				    ),
					            				);
					            				$svg_file = file_get_contents($icon['url'], false, stream_context_create($arrContextOptions));
					            				$find_string   = '<svg';
					            				$position = strpos($svg_file, $find_string);
					            				$svg_file_new = substr($svg_file, $position);

					            				echo $svg_file;
					            			} 
					            		?>
					            	</div>
					            </div>
					        </a>
				            <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
				        </div>
				    <?php endwhile; ?>
			    </div>
			<?php endif; wp_reset_postdata(); ?>
		</div>
	</div>
</section>