<?php $background_colour = get_sub_field('background_colour'); ?>

<section class="block-call-to-action-image">
	<div class="row columns">
		<div class="background-color <?php echo $background_colour; ?>">
			<div class="row columns" data-equalizer>
				<div class="large-12 columns large-push-2" data-equalizer-watch>
					<?php echo get_sub_field('content')['content']; ?>
					<?php if(have_rows('button_group')) : ?>
						<?php while(have_rows('button_group')) : the_row(); ?>
							<div class="button-group stacked-for-small">
								<?php get_template_part('parts/global/button', 'group'); ?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div class="row hide-for-large"><div class="columns section-divider xsmall-divider">&nbsp;</div></div>
				<div class="small-20 medium-14 large-10 columns small-centered large-uncentered text-center vertical-align middle" data-equalizer-watch>
					<img class="image" src="<?php echo get_sub_field('image')['sizes']['landscape_500x400_no_crop']; ?>" alt="<?php echo get_sub_field('image')['alt']; ?>" width="<?php echo get_sub_field('image')['sizes']['landscape_500x400_no_crop-width']; ?>" height="<?php echo get_sub_field('image')['sizes']['landscape_500x400_no_crop-height']; ?>">
				</div>
			</div>
		</div>
	</div>
</section>