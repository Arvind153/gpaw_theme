<section class="block-feeding-steps overlap-section-below">
	<div class="row steps" data-equalizer>
		<div class="small-12 medium-8 large-4 columns text-center step end" data-equalizer-watch>
			<div class="circle">
				<div class="content">
					<?php echo get_sub_field('step_1'); ?>
					<div class="number">1</div>
				</div>
			</div>
		</div>
		<div class="show-for-large large-1 columns text-center spacer end" data-equalizer-watch>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/plus.png" alt="plus">
		</div>
		<div class="small-12 medium-8 large-4 columns text-center step end" data-equalizer-watch>
			<div class="circle">
				<div class="content">
					<?php echo get_sub_field('step_2'); ?>
					<div class="number">2</div>
				</div>
			</div>
		</div>
		<div class="show-for-large large-1 columns text-center spacer end" data-equalizer-watch>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/plus.png" alt="plus">
		</div>
		<div class="small-12 medium-8 large-4 columns text-center step end" data-equalizer-watch>
			<div class="circle">
				<div class="content">
					<?php echo get_sub_field('step_3'); ?>
					<div class="number">3</div>
				</div>
			</div>
		</div>
		<div class="show-for-large large-1 columns text-center spacer end" data-equalizer-watch>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/plus.png" alt="plus">
		</div>
		<div class="small-12 medium-8 large-4 columns text-center step end" data-equalizer-watch>
			<div class="circle">
				<div class="content">
					<?php echo get_sub_field('step_4'); ?>
					<div class="number">4</div>
				</div>
			</div>
		</div>
		<div class="show-for-large large-1 columns text-center spacer end" data-equalizer-watch>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/plus.png" alt="plus">
		</div>
		<div class="small-12 medium-8 large-4 columns text-center step end" data-equalizer-watch>
			<div class="circle">
				<div class="content">
					<?php echo get_sub_field('step_5'); ?>
					<div class="number">5</div>
				</div>
			</div>
		</div>
	</div>
</div>