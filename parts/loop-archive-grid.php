<?php
/**
 * The template part for displaying a grid of posts
 *
 * For more info: http://jointswp.com/docs/grid-archive/
 */

// Adjust the amount of rows in the grid
$grid_columns = 3; ?>

<?php if( 0 === ( $wp_query->current_post  )  % $grid_columns ): ?>

    <div data-equalizer="inner-content-title"> <!--Begin Grid--> 
		<div class="row category-grid" data-equalizer="inner-content-excerpt">

<?php endif; ?> 

			<!--Item: -->
			<div class="large-12 xlarge-8 columns panel end category-post" data-equalizer-watch="outer-content">
			
				<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

					<?php
						$image_id = get_post_thumbnail_id(get_the_ID());
						$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);
					?>
			
					<section class="featured-image" itemprop="text">
						<div class="date">
							<small><?php echo get_the_date('d M'); ?></small>
							<?php echo get_the_date('Y'); ?>
						</div>
						
						<?php
							$image_id = get_post_thumbnail_id(get_the_ID());

							if($image_id) {
								$image_alt = get_post_meta($image_id , '_wp_attachment_image_alt', true);
								$image_url = get_the_post_thumbnail_url(get_the_ID(), 'landscape_500x400');
					        	$image_attributes = wp_get_attachment_metadata($image_id);

					        	if($image_attributes) {
					        		$image_width = $image_attributes['sizes']['landscape_500x400']['width'];
					        		$image_height = $image_attributes['sizes']['landscape_500x400']['height'];
					        	}
							} else {
								$image_alt = get_field('page_layouts', $post->ID)[0]['background_image']->alt;
								$image_url = get_field('page_layouts', $post->ID)[0]['background_image']['sizes']['landscape_500x400'];
					        	$image_width = get_field('page_layouts', $post->ID)[0]['background_image']['sizes']['landscape_500x400-width'];
					        	$image_height = get_field('page_layouts', $post->ID)[0]['background_image']['sizes']['landscape_500x400-height'];
							}
						?>
						<img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>">
					</section>
				
					<header class="article-header" data-equalizer-watch="inner-content-title">
						<h4 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>	
					</header> <!-- end article header -->	
									
					<section class="entry-content" itemprop="text" data-equalizer-watch="inner-content-excerpt" style="padding-bottom: rem;">
						<?php the_excerpt(); ?>
					</section> <!-- end article section -->

					<footer>
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="button expanded">Read more</a>
					</footer>
									    							
				</article> <!-- end article -->
				
			</div>

<?php if( 0 === ( $wp_query->current_post + 1 )  % $grid_columns ||  ( $wp_query->current_post + 1 ) ===  $wp_query->post_count ): ?>
		
		</div>  <!--End Grid --> 
	</div>  <!--End Grid --> 

<?php endif; ?>

