<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
	<header>
		<div class="row "><div class="columns section-divider small-divider">&nbsp;</div></div>

		<div class="row">
			<div class="columns text-center">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</header>

	<div class="row "><div class="columns section-divider medium-divider">&nbsp;</div></div>

	<div class="row">
		<div class="columns">
			
			<?php the_content(); ?>
		</div>
	</div>

	<div class="row "><div class="columns section-divider medium-divider">&nbsp;</div></div>
					
</article> <!-- end article -->