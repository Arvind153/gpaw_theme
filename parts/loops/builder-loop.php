<?php if(have_rows('page_layouts')) : ?>
	<?php while(have_rows('page_layouts')): the_row(); ?>
		<?php get_template_part('parts/blocks/' . get_row_layout()); ?>
	<?php endwhile; ?>
<?php endif; ?>