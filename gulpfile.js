var { task, watch, src, dest, series } = require('gulp'),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify')
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
    touch = require('gulp-touch'),
    t2 = require('through2'),
    filter = require('gulp-filter'),
    merge = require('merge-stream'),
    touch = require('gulp-touch-cmd');

const FOUNDATION = "node_modules/foundation-sites";

const paths = {
    styles: {
        src: "assets/styles/scss/**/*.scss",
        dest: "assets/styles"
    }, 
    scripts: {        
        src: [
		    "node_modules/what-input/dist/what-input.js",

			// Foundation core - needed if you want to use any of the components below
			FOUNDATION + "/dist/js/plugins/foundation.core.js",
			FOUNDATION + "/dist/js/plugins/foundation.util.*.js",

			// Pick the components you need in your project
			// FOUNDATION + "/dist/js/plugins/foundation.abide.js",
			FOUNDATION + "/dist/js/plugins/foundation.accordion.js",
			FOUNDATION + "/dist/js/plugins/foundation.accordionMenu.js",
			FOUNDATION + "/dist/js/plugins/foundation.drilldown.js",
			FOUNDATION + "/dist/js/plugins/foundation.dropdown.js",
			FOUNDATION + "/dist/js/plugins/foundation.dropdownMenu.js",
			FOUNDATION + "/dist/js/plugins/foundation.equalizer.js",
			FOUNDATION + "/dist/js/plugins/foundation.interchange.js",
			FOUNDATION + "/dist/js/plugins/foundation.offcanvas.js",
			// FOUNDATION + "/dist/js/plugins/foundation.orbit.js",
			FOUNDATION + "/dist/js/plugins/foundation.responsiveMenu.js",
			FOUNDATION + "/dist/js/plugins/foundation.responsiveToggle.js",
			FOUNDATION + "/dist/js/plugins/foundation.reveal.js",
			// FOUNDATION + "/dist/js/plugins/foundation.slider.js",
			// FOUNDATION + "/dist/js/plugins/foundation.smoothScroll.js",
			// FOUNDATION + "/dist/js/plugins/foundation.magellan.js",
			FOUNDATION + "/dist/js/plugins/foundation.sticky.js",
			FOUNDATION + "/dist/js/plugins/foundation.tabs.js",
			FOUNDATION + "/dist/js/plugins/foundation.responsiveAccordionTabs.js",
			// FOUNDATION + "/dist/js/plugins/foundation.toggler.js",
			// FOUNDATION + "/dist/js/plugins/foundation.tooltip.js",

            // "assets/vendors/slick-lightbox/dist/slick-lightbox.min.js",
            // "assets/vendors/slick-slider/slick/slick.js",
            // "assets/vendors/svg-convert/dist/svgConvert.js",
            // "assets/vendors/jquery.cookie/jquery.cookie.js",
            // "assets/vendors/magnify/dist/js/jquery.magnify.js",
            // "assets/vendors/TinyNav.js/tinynav.min.js",

			"assets/scripts/js/**/*.js",
        ],
        dest: "assets/scripts"
    },
};

const ASSETS = {
	styles: "assets/styles/",
	scripts: "assets/scripts/",
	images: "assets/images/",
	all: "assets/"
};

const JSHINT_CONFIG = {
	"node": true,
	"globals": {
		"document": true,
		"window": true,
		"jQuery": true,
		"$": true,
		"Foundation": true
	}
};

sass.compiler = require("node-sass");

const vendors = ['foundation-sites', 'jquery', 'what-input'];

function bundle() {
  return merge(vendors.map(function(vendor) {
    return src('node_modules/' + vendor + '/**/*')
      .pipe(dest('assets/vendors/' + vendor.replace(/\/.*/, '')));
  }));
}

function scripts() {
   return src(paths.scripts.src)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('scripts.js'))
        .pipe(sourcemaps.write("."))
        .pipe(dest(paths.scripts.dest));
};

function scss(cb) {
    src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: "expanded"}))
        .on("error", sass.logError)
        .pipe(postcss([autoprefixer(), cssnano({safe: true, minifyFontValues: {removeQuotes: false}})]))
        .pipe(t2.obj((chunk, enc, cb) => {
            let date = new Date();
            chunk.stat.atime = date;
            chunk.stat.mtime = date;
            cb(null, chunk);
        }))
        .pipe(sourcemaps.write("."))
        .pipe(dest(paths.styles.dest))
        .pipe(touch());
    cb();
}

function watcher(cb) {
    watch(paths.styles.src, series(scss))
    watch(paths.scripts.src, series(scripts))
    cb();
}

module.default = task("default", series([scss, scripts, watcher, bundle]));