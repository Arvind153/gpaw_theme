<?php
/**
 * The template for displaying search form
 */
 ?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="input-group">
		<input type="search" class="input-group-field search-field" placeholder="<?php echo esc_attr_x( 'Search...', 'jointswp' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointswp' ) ?>" />
		<div class="input-group-button">
			<button type="submit" class="button search-submit">
				<?php echo esc_attr_x( 'Search', 'jointswp' ) ?>
			</button>
		</div>
	</div>
</form>